<?php
/**
 * @package WooCommerce_Custom_Email_Footer
 * @version 1.0
 */
/*
Plugin Name: WooCommerce Custom Email Footer
Plugin URI: https://advantinteractive.com
Description: Extend basic WooCommerce Email Footer.
Author: Bartek Makowski
Version: 1.0
Author URI: https://advantinteractive.com
*/
add_action('woocommerce_email_footer', 'gh_custom_email_footer', 10, 1);
function gh_custom_email_footer($email)
{
    if ($email && ('customer_processing_order' == $email->id
        || 'processing_renewal_order' == $email->id)) {
        echo '<p>IF YOU HAVE SIGNED UP FOR A “MONTHLY” or “SUBSCRIPTION” order, you have agreed to receive one or more Whole Body Research products on a recurring basis and that your credit card will be charged the amount matching your selected package size, billed to your card as “Whole Body Research.” You can stop shipments anytime by calling customer service at 1-800-240-7721, from 5am to 7pm PT M-F & 6am to 4:30pm PT S-Su, or by email at <a href="mailto:support@wholebodyresearch.com">support@wholebodyresearch.com</a>.</p>';
    }
}
